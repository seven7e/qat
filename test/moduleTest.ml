open OUnit2
open Module
open ModuleManager
open Ast

let assert_module c expect got =
    assert_equal ~ctxt:c
        ~cmp:module_equals
        ~printer:str_of_module_verbose
        expect got
;;

let assert_equal_str c expect got =
    assert_equal ~ctxt:c ~printer:(fun s -> s) expect got
;;

let i x = Atom (Id x);;
let op x = Atom (Op x);;
let ls x = NodeList x;;

Random.self_init();;

let suite =
    "module" >::: [
        "test_add_module" >:: (fun c ->
            let mng = create_module_manager () in
            let name = ["foo"; "bar"] in
            let m = create_module name in
            add_module mng m 0;
            let m2 = get_module mng name in
            assert_module c m m2);

        "test_add_code" >:: (fun c ->
            let mng = create_module_manager () in
            let name = ["foo"; "bar"] in
            let code = "(x + y)" in
            let ast = ls [i "x"; op "+"; i "y";] in
            let m = create_module_with_code name code in
            let m2 = create_module_with_code name code in
            m2.ast <- Some ast;
            assert_module c m m2);

        "test_modulize_naive" >:: (fun c ->
            let create mdl = 2 in
            let proc mp state num = num + state in
            let mp = modulize create proc in
            let out = modulized_process mp 10 in
            Printf.printf "%s\n" (string_of_int out));

        "test_modulize" >:: (fun c ->
            let create mdl :int =
                let i = Random.int 10 in
                Printf.printf "random for module %s: %d\n"
                    (Basic.str_of_fullname mdl.name) i;
                i
            in
            let proc (mp :(int, float, string) modulized_processor_t)
                    state num =
                let mdl_state :int = import_module mp ["foo"] in
                Printf.sprintf "%d(foo) %d -> %.3f" mdl_state state num
            in
            let mp = modulize create proc in
            let out = modulized_process mp 10. in
            Printf.printf "%s\n" out;
            assert_equal_str c "xx" out
        );


    ]
