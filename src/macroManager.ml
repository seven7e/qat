open Ast;;
open Macro;;
module DA = DynArray;;

type 'm macro_manager = {
    prcdn: 'm precedences; (* macro precedence hierarchy *)
    macros_defs: (('m macro) * macro_precedence) DA.t
}
;;

let start_symbol = "E";;

let create_macro_manager () :macro_elem macro_manager =
    {prcdn=make_precedences ();
    macros_defs=DA.make 100
}
;;

let show_macro_manager mmngr :string =
    "========= Macros =========\n"
    ^ str_of_precedences mmngr.prcdn
    ^ "\n========== Macro definitions ==========\n"
    ^ let f (m, p) = str_of_macro_def m p in
        Util.join_da "\n" (DA.map f mmngr.macros_defs)
;;

let add_macro (mmngr :'m macro_manager)
        (mcr :'m macro)
        (prec :macro_precedence)
        :unit =
    DA.add mmngr.macros_defs (mcr, prec);
    Macro.add_macro mmngr.prcdn mcr prec
;;

let get_macro mmngr =
    Macro.get_macro mmngr.prcdn
;;

