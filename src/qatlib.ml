open Ast
open Parse
open MacroManager
open MacroGrammarBuilder
open Expand
open Sementic
open Evaluate

type interp = {
    mbldr: (Macro.macro_elem, Ast.ast) macro_grammar_builder;
    evaluator: evaluator}
;;

let make_interp () =
    {mbldr = create_macro_grammar_builder ();
    evaluator = make_evaluator () }
;;

let run interp (code :string) :evalret =
    code |> parse
        |> (expand interp.mbldr)
        |> sementic_analyze
        |> (evaluate interp.evaluator)
;;

let run_no_macro interp (code :string) :evalret =
    code |> parse
        |> sementic_analyze
        |> (evaluate interp.evaluator)
;;

let run_parse (code :string) :string =
    code |> parse |> pretty_sexp_of_ast
;;

let run_expand interp (code :string) :string =
    code |> parse
        |> (expand interp.mbldr)
        |> pretty_sexp_of_ast
;;

