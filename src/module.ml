open Ast
module DA = DynArray;;

type module_name_t = Basic.fullname_t;;

type module_t = {
    name: module_name_t;
    path: string;
    mutable code: string;
    mutable ast: ast option;
    (*expanded: ast option;*)
    (*pre_evaluated: estmt option;*)
}
;;

let create_module (name :module_name_t) :module_t =
    {name=name; path="TODO"; code=""; ast=None}
;;

let add_code (m :module_t) (text: string) :unit =
    m.code <- text;
    m.ast <- Some (Parse.parse text)
;;

let create_module_with_code name code =
    let m = create_module name in
    add_code m code;
    m
;;

let str_of_module m :string =
    "Module(" ^ String.concat "." m.name ^ ")"
;;

let str_of_module_verbose m :string =
    let ast_str = match m.ast with
        | None -> ""
        | Some a -> str_of_ast a
    in
    "Module(name=" ^ String.concat "." m.name
        ^ ",code=" ^ m.code
        ^ ",ast=" ^ ast_str
        ^ ")"
;;

let module_equals m1 m2 :bool =
    m1.name = m2.name && m1.path = m2.path && m1.code = m2.code
    && m1.ast = m2.ast
;;
