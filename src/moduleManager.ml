open Module
open Env
open Basic
module DA = DynArray;;

exception ModuleErr of string

type 'state module_manager_t = {
    pathes: string DA.t;
    moduleDict: (module_name_t, module_t * 'state) Hashtbl.t}
;;

(* ------------ module manager ------------- *)
let create_module_manager () :'s module_manager_t =
    {pathes=DA.make 1; moduleDict=Hashtbl.create 1}
;;

let has_module (mng :'s module_manager_t) (name :module_name_t) :bool =
    Hashtbl.mem mng.moduleDict name
;;

let add_module (mng :'s module_manager_t) (mdl :module_t) (state :'s) =
    if has_module mng mdl.name then
        raise (ModuleErr ("Module " ^ str_of_fullname mdl.name ^ " already exist!"))
    else
        Hashtbl.add mng.moduleDict mdl.name (mdl, state)
;;

let get_module_entry (mng :'s module_manager_t) (name :module_name_t)
    :module_t * 's =
    Hashtbl.find mng.moduleDict name
;;

let get_module mng name = fst (get_module_entry mng name)

let get_module_extra mng name = snd (get_module_entry mng name)

let find_source_file mng name = ()
;;

let load_module (mng :'s module_manager_t) (name :string) :unit =
    let file = find_source_file mng name in
    let text = Util.read_file name in
    ()
;;

(* ------------------- modulization ------------------ *)

type ('s, 'i, 'o) modulized_processor_t = {
    module_manager: ('s module_manager_t);
    create: (module_t -> 's);
    (* get_input: (fullname_t -> 's); *)
    process: (('s, 'i, 'o) modulized_processor_t -> 's -> 'i -> 'o);
};;

let import_module (mp :('s, 'i, 'o) modulized_processor_t) (name :fullname)
        : 's =
    (* let mdl = create_module name in *)
    let () = if not (has_module mp.module_manager name) then
        let mdl = create_module name in
        let state = mp.create mdl in
        add_module mp.module_manager mdl state
    in
    let mdl = get_module_entry mp.module_manager name in
    snd mdl
;;

let modulized_process (mp :('s, 'i, 'o) modulized_processor_t)
        inp : 'o =
    (* let mdl = get_module mp name in *)
    let mdl = create_module ["__main__"] in
    let state = mp.create mdl in
    mp.process mp state inp
;;

let modulize
        (create: module_t -> 's)
        (* (get_input: fullname_t -> 'i) *)
        (process: ('s, 'i, 'o) modulized_processor_t -> 's -> 'i -> 'o)
        : ('s, 'i, 'o) modulized_processor_t =
    let mp = {
        module_manager = create_module_manager();
        create = create;
        (* get_input = get_input; *)
        process = process;
    }
    in
    mp
;;
