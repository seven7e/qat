open Ast;;
open Macro;;
open MacroManager;;
open MacroGrammarBuilder;;
open Earley;;
module DA = DynArray;;
module StrMap = Util.StrMap;;
(* open Module *)
(* open ModuleManager *)

type expander = {
    mbldr: (macro_elem, Ast.ast) macro_grammar_builder;
    (* mdl_mngr: module_manager *)
}

let v s = Variable s;;
let ls s = Literal (Id s);;
let lo s = Literal (Op s);;

let add_macro = MacroManager.add_macro;;
let get_macro = MacroManager.get_macro;;

let get_macro_of_rule mbldr i =
    Macro.get_macro mbldr.mmngr.prcdn (Hashtbl.find mbldr.rules_for_macro i)
;;

let rec parse_tree_to_ast (tree :'a parse_tree) :ast =
    match tree with
    | Leaf lf -> lf
    | Tree (i, arr) ->
            NodeList (Array.to_list (Array.map parse_tree_to_ast arr))
;;

let rec simplify_parse_tree mbldr ptree :'a parse_tree =
    let f = simplify_parse_tree mbldr in
    match ptree with
    | (Leaf _) as lf -> lf
    | Tree (i, arr) ->
            if is_macro mbldr i then
                Tree (i, (Array.map f arr))
            else if Array.length arr <> 1 then
                raise (MacroErr
                "Grammar rules not for a macro should have a length-one rhs")
            else
                f (Array.get arr 0)
;;

let str_of_ast_array arr =
    "[" ^ (Util.joina ", " (Array.map str_of_ast arr)) ^ "]"
;;

let parse_pattern_raw mbldr exp :'a parse_tree =
    let arr = match exp with
        | Atom a -> raise (MacroErr "Input should be a list, not an atom")
        | NodeList el -> Array.of_list el
    in
    (*Util.println (str_of_items str_of_ast mbldr.gram (earley_match mbldr.gram arr));*)
    try parse mbldr.gram arr
    with EarleyErr i ->
        let ierr = i - 1 in
        raise (MacroErr ("macro expanding error at token " ^ string_of_int i
            ^ ": "
            ^ str_of_ast_array (Array.sub arr ierr (Array.length arr - ierr))))
;;

let parse_pattern mbldr exp :'a parse_tree =
    simplify_parse_tree mbldr (parse_pattern_raw mbldr exp)
;;

let rec extract_vars_list (patt_list :macro_ast list)
                      (ast_list :ast list)
                      :ast StrMap.t =
    match patt_list, ast_list with
    | [], [] -> StrMap.empty
    | p::ps, e::es ->
            let mmap_first = extract_vars p e in
            let mmap_rest = extract_vars_list ps es in
            Util.merge_str_map mmap_first mmap_rest
    | _ -> raise (MacroErr "")
and extract_vars_atom (patt :macro_elem) (exp :ast) :ast StrMap.t =
    match patt, exp with
    | Literal lit, Atom a when lit = a -> StrMap.empty
    | Variable v, _ -> StrMap.add v exp StrMap.empty
    | _ -> raise (MacroErr "")
and extract_vars (pattern :macro_ast) (exp :ast) :ast StrMap.t =
    match pattern, exp with
    | Atom a, e -> extract_vars_atom a e
    (*TODO: change to foldr *)
    | NodeList pl, NodeList el -> extract_vars_list pl el
    | _ -> raise (MacroErr "")
;;

let rec substitute_vars (vars :ast StrMap.t) (body :macro_ast) :ast =
    match body with
    | Atom a ->
            (match a with
            | Literal lit -> Atom lit
            | Variable v -> StrMap.find v vars)
    | NodeList ml -> NodeList (List.map (substitute_vars vars) ml)
;;

let expand_macro (mcr :'m macro) (exp :ast) :ast =
    let vars = extract_vars mcr.pattern exp in
    (*Printf.printf "var mapping: %s\n" (Util.str_of_strmap str_of_ast vars);*)
    substitute_vars vars mcr.body
;;

let rec expand_non_macro mbldr i arr :ast =
    if Array.length arr <> 1 then
        raise (MacroErr
            "Grammar rules not for a macro should have a length-one rhs")
    else
        expand_parse_tree mbldr (Array.get arr 0)
and expand_rule mbldr i arr :ast =
    if is_macro mbldr i then
        let f = expand_parse_tree mbldr in
        expand_macro (get_macro_of_rule mbldr i)
                (* expand inner first, i.e. depth-first *)
                (NodeList (Array.to_list (Array.map f arr)))
    else
        expand_non_macro mbldr i arr
and expand_parse_tree mbldr ptree :ast =
    match ptree with
    | Leaf lf -> lf
    | Tree (i, arr) -> expand_rule mbldr i arr
;;

let expand_one_level mbldr stmt =
    let tree = parse_pattern mbldr stmt in
    expand_parse_tree mbldr tree
;;

let compute_assoc (assoc_stmt :ast option) :associativity option =
    match assoc_stmt with
    | None -> None
    | Some (NodeList [Atom (Id "associativity"); Atom (Id assoc_str)]) ->
        let assoc = match assoc_str with
            | "left" -> Left
            | "right" -> Right
            | "none" -> Non
            | _ -> raise (MacroErr "Invalid Associativity")
        in
        Some assoc
    | _ -> raise (MacroErr "")
;;

(*TODO: can prefix and postfix be non-associated?*)
let compute_fixity assoc_stmt pattern =
    (*let _ = match assoc_stmt with*)
    (*| None -> Printf.printf "None\n"*)
    (*| Some s -> Printf.printf "%s\n" (str_of_ast s)*)
    (*in*)
    let get_atom node =
        match node with
        | Atom a -> a
        | NodeList _ -> raise (MacroErr "Element in macro pattern must be atom")
    in
    let assoc = compute_assoc assoc_stmt in
    match pattern with
    | NodeList nl -> (match (get_atom (List.hd nl),
            get_atom (Util.list_last nl)) with
        (*match (List.hd pattern, Util.list_last pattern) with*)
        | Literal _, Variable _ -> (match assoc with
            | None | Some Right -> Prefix
            | _ -> raise (MacroErr "Prefix can only be right associated"))
        | Variable _, Variable _ -> (match assoc with
            | Some a -> Infix a
            | _ -> raise (MacroErr
                "Associativity must be provieded for infix operator"))
        | Variable _, Literal _ -> (match assoc with
            | None | Some Left -> Postfix
            | _ -> raise (MacroErr "Postfix can only be left associated"))
        | Literal _, Literal _ -> (match assoc with
            | None | Some Non -> Closed
            | _ -> raise (MacroErr "Closed can only be none associated")))
    | _ -> raise (MacroErr "")
;;

let pre_macro_body (body :ast) :ast =
    let rec flist a =
        match a with
        | [] -> []
        | (Atom (Op "?") as q)::(Atom (Id s) as v)::res ->
            NodeList [q; v]::flist res
        | x::xs -> fa x::flist xs
    and fa a =
        match a with
        | NodeList xs -> NodeList (flist xs)
        | (Atom _ ) as aa -> aa
    in
    fa body
;;

let ast_to_m_ast stmt :macro_ast =
    let rec trans_rev_stmt e head_rev =
        match e with
        | [] -> head_rev
        | Atom (Op "?")::Atom (Id opd)::rest ->
            trans_rev_stmt rest (Atom (Variable opd)::head_rev)
        | Atom opr::rest ->
            trans_rev_stmt rest (Atom (Literal opr)::head_rev)
        | _ -> raise (MacroErr "MACRO element invalid")
    in
    match stmt with
    | NodeList nl -> NodeList (List.rev (trans_rev_stmt nl []))
    | _ -> raise (MacroErr "MACRO pattern/body should be a statement")
;;

let body_to_m_ast stmt :macro_ast =
    let rec trans_rev_stmt e head_rev =
        match e with
        | [] -> head_rev
        | NodeList [Atom (Op "?"); Atom (Id opd)]::rest ->
            trans_rev_stmt rest (Atom (Variable opd)::head_rev)
        | Atom opr::rest ->
            trans_rev_stmt rest (Atom (Literal opr)::head_rev)
        | _ -> raise (MacroErr "MACRO element invalid")
    in
    match stmt with
    | NodeList nl -> NodeList (List.rev (trans_rev_stmt nl []))
    | _ -> raise (MacroErr "MACRO pattern/body should be a statement")
;;

let stmt_to_macro_id stmt :macro_id =
    let f n =
        match n with
        | Atom (Id "_") -> Placeholder
        | Atom (Id s) | Atom (Op s) -> MName s
        | _ -> raise (MacroErr
            "Macro name element must be identifier or operator)")
    in
    match stmt with
    | NodeList nl -> List.map f nl
    | _ -> raise (MacroErr "Invalid macro name")
;;

let compute_precedence stmt :macro_precedence =
    let rec f key s =
        let ret = match s with
            | [] | [_] -> None
            | Atom (Id k)::v::_ when k = key -> Some (stmt_to_macro_id v)
            | _::_::rest -> f key rest
        in
        (*Printf.printf "key: %s -> %s\n" key (match xx with |None -> "none"*)
            (*| Some n -> str_of_macro_id n);*)
        ret
    in
    (*Printf.printf "stmt: %s\n" (str_of_ast stmt);*)
    match stmt with
    | NodeList (Atom (Id "precedence")::tail) ->
        (match f "equals" tail, f "lowerThan" tail, f "higherThan" tail with
        | Some eq, None, None -> MEquals eq
        | None, high, low -> MBetween (high, low)
        | _ -> raise (MacroErr "DEFMACRO: invalid precedence"))
    | _ -> raise (MacroErr "DEFMACRO: invalid precedence")
;;

let define_macro mmngr
        (assoc_stmt :ast option)
        (preced_stmt :ast)
        (pattern_stmt :ast)
        (body_stmt :ast)
        :('m macro * macro_precedence) =
    let pattern = ast_to_m_ast pattern_stmt in
    let fix = compute_fixity assoc_stmt pattern in
    let pre = compute_precedence preced_stmt in
    let m = new_macro fix pattern (body_to_m_ast body_stmt)
    in
    add_macro mmngr m pre;
    m, pre
;;

let import_macro mmngr opd :unit =
    match opd with
    | [Atom (Id mname)] -> ()
    | _ -> raise (MacroErr "IMPORTM: invalid syntax")
;;

(*seems `macro_list` is not necessary but I'm lazy to remove it for now*)
let rec extract_and_expand mbldr (stmt :ast)
        :ast * ('m macro * macro_precedence) list =
    let helper ass pre_stmt pat_stmt body_stmt =
        let body_exp, macro_list
                = extract_and_expand mbldr (pre_macro_body body_stmt) in
        (*Util.println (show_macro_manager mmngr);*)
        Util.println (str_of_ast body_exp);
        let m, pre = define_macro mbldr.mmngr ass pre_stmt pat_stmt body_exp in
        (*TODO: build grammar increamentally instead of rebuild all for efficiency*)
        build_grammar mbldr;
        (*Util.println (show_macro_grammar_builder mbldr);*)
        (*NodeList [Atom (Id "nil")]*)
        Atom (Id "nil"), (m, pre)::macro_list
    in
    (*let _ = Printf.printf "%s\n" (str_of_ast stmt) in*)
    match stmt with
    | (Atom _) as a -> a, []
    | NodeList (Atom (Id "defmacro")::tail) -> (
        let _ = Printf.printf "define macro: %s\n" (str_of_ast (NodeList tail)) in
        let res = match tail with
            | [pattern_stmt; preced_stmt; body_stmt] ->
                helper None preced_stmt pattern_stmt body_stmt
            | [pattern_stmt; assoc_stmt; preced_stmt; body_stmt] ->
                (*Printf.printf "%s\n" (str_of_ast assoc_stmt);*)
                helper (Some assoc_stmt) preced_stmt pattern_stmt body_stmt;
            | _ -> raise (MacroErr "DEFMACRO: invalid syntax")
        in
        (*let _ = Printf.printf "%s\n" (str_of_ast res) in*)
        res
        )
    | NodeList (Atom (Id "importm")::tail) -> (
        let _ = import_macro mbldr.mmngr tail in
        Atom (Id "nil"), [])
        (* FIXME: empty list is not correct. Return all macros imported *)
    | NodeList nl -> let stmt_and_macros =
                (List.map (extract_and_expand mbldr) nl) in
        let deep_expanded =
                NodeList (List.map (fun (e, _) -> e) stmt_and_macros) in
        let macro_list =
                List.fold_left (fun a (_, m) -> a @ m) [] stmt_and_macros in
        (*let _ = Printf.printf "deep expanded: %s\n" (str_of_ast deep_expanded) in*)
        let expanded_stmt =
            try
                expand_one_level mbldr deep_expanded
            with
                (*| MacroErr _ -> stmt*)
                | MacroErr _ -> deep_expanded
        in
        expanded_stmt, macro_list
;;

let expand mbldr (stmt :ast) :ast =
    match extract_and_expand mbldr stmt with
    | (s, _) -> s
;;
